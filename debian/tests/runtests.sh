#!/bin/sh

set -e

echo "Start postgresql service to init database"
systemctl start postgresql

echo "Init database"
sudo faraday-manage initdb
sleep 10

echo "Start faraday service"
sudo systemctl start faraday

echo "Check if faraday service is active"
if ! systemctl is-active -q faraday; then
    echo "the service faraday is not active"
    exit 1
fi
